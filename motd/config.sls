{%- from slspath+'/map.jinja' import motd with context -%}

motd_file_update:
  file.managed:
    - name: {{ motd.filename }}
    - backup: minion
    - source: salt://{{ slspath }}/files/motd.jinja
    - template: jinja
    - replace: True
    - user: root
    - group: root
    - mode: 0644
